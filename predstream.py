import streamlit as st
import pandas as pd

# Import the predict function from the previous code block
from var_model import predict

# Set the title of the app
st.title("VAR Model Predictions")

# Add a short description
st.write("Enter a start and end date to get predictions for the next 12 months.")

# Add input fields for the start and end dates
start_date = st.date_input("Start date", value=pd.to_datetime("2022-01-01"))
end_date = st.date_input("End date", value=pd.to_datetime("2023-01-01"))

# Add a button to make the predictions
if st.button("Get Predictions"):
    # Call the predict function with the input dates
    predictions = predict(start_date, end_date)

    # Display the predicted values in a table
    st.write(predictions)
