import yfinance as yf
import numpy as np
import pandas as pd
from statsmodels.tsa.api import VAR

tickers = ["AAPL", "MSFT", "GOOG", "AMZN", "META"]

# Import data from Yahoo Finance and set group_by to 'ticker'
data = yf.download(tickers, start="2020-01-01", end="2022-01-01", group_by='ticker')

# Reset the index to add the date column as a separate column
data = data.reset_index()
df = pd.DataFrame()

for ticker in tickers:
    df[ticker] = data[ticker]['Close']

date1 = data['Date']
df = pd.concat([date1, df], axis=1)
df.to_csv('stocks.csv')
df = pd.read_csv('stocks.csv', parse_dates=['Date'], index_col='Date')

# Select the columns to use for forecasting
columns = ['AAPL', 'MSFT', 'GOOG', 'AMZN','META']
data = df[columns]

# Fit a VAR model to the data
model = VAR(data)
results = model.fit()

# Define a function to make predictions
def predict(start, end):
    # Create a date range based on the start and end dates
    date_range = pd.date_range(start=start, end=end)

    # Use the model to make predictions for the date range
    predictions = results.forecast(data.values[-results.k_ar:], len(date_range))

    # Convert the predictions to a DataFrame and set the column names
    preds_df = pd.DataFrame(predictions, index=date_range, columns=columns)

    # Return the predictions DataFrame
    return preds_df

import streamlit as st
import pandas as pd

# Import the predict function from the previous code block
from var_model import predict

# Set the title of the app
st.title("VAR Model Predictions")

# Add a short description
st.write("Enter a start and end date to get predictions for the next 12 months.")

# Add input fields for the start and end dates
start_date = st.date_input("Start date", value=pd.to_datetime("2022-01-01"))
end_date = st.date_input("End date", value=pd.to_datetime("2023-01-01"))

# Add a button to make the predictions
if st.button("Get Predictions"):
    # Call the predict function with the input dates
    predictions = predict(start_date, end_date)

    # Display the predicted values in a table
    st.write(predictions)
